package br.com.teste.demoteste.util;

import java.util.ArrayList;
import java.util.List;

import br.com.teste.demoteste.dto.TipoOperacaoDTO;

public enum TipoOperacaoEnum {

	READ_COILS("01"),
	READ_DISCRETE_INPUTS("02"),
	READ_HOLDING_REGISTERS("03"),
	READ_INPUT_REGISTERS("04"),
	WRITE_SINGLE_COIL("05"),
	WRITE_SINGLE_REGISTER("06"),
	WRITE_MULTIPLE_COILS("0F"),
	WRITE_MULTIPLE_REGISTERS("10"),
	MASK_WRITE_REGISTER("16"),
	READ_WRITE_MULTIPLE_REGISTER("17");

	private final String descricao;

	private TipoOperacaoEnum(String descricao) {
		this.descricao = descricao;
	}
	public String getDescricao() {
		return this.descricao;
	}

	public static List<TipoOperacaoDTO> toDTO() {

		ArrayList<TipoOperacaoDTO> lista = new ArrayList<>();

		for (TipoOperacaoEnum tipoPublico : values()) {
			lista.add(new TipoOperacaoDTO(tipoPublico.name(), tipoPublico.getDescricao()));
		}

		return lista;
	}

}