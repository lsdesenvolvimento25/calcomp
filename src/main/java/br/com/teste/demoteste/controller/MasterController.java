package br.com.teste.demoteste.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import br.com.teste.demoteste.dto.ValoresDTO;
import br.com.teste.demoteste.service.MasterService;
import lombok.extern.log4j.Log4j2;





@RestController
@RequestMapping("/api/v1/modbus")
@Log4j2
@Validated
public class MasterController {

	@Autowired
	private MasterService service;
	
	@ResponseBody
	@RequestMapping(value="/escrita", method = RequestMethod.POST, produces = "application/json", consumes = "application/json")
	public ResponseEntity<Object> solicitacaoExameMedicoPsicotecnico(@RequestBody ValoresDTO valor) throws Exception {
		log.info("escrevendo");

		service.escritaSimples(valor.getEndereco(), valor.getValor());
		
		return null;
	}
    
}