package br.com.teste.demoteste.dto;

import java.io.Serializable;

public class TipoOperacaoDTO implements Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	private String codigo;
	private String funcao;


	public TipoOperacaoDTO(String codigo, String funcao) {
		this.codigo = codigo;
		this.funcao = funcao;
	}

	public TipoOperacaoDTO() {
		
	}
	
}