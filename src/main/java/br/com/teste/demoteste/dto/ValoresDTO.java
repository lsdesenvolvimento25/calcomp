package br.com.teste.demoteste.dto;

import java.io.Serializable;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter @Setter @NoArgsConstructor 
public class ValoresDTO implements Serializable{
	
	private static final long serialVersionUID = -1764970284520387975L;
	
	private int endereco;
	private int valor;
	
	
	
	
}
