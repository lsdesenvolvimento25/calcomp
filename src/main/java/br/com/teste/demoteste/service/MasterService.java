package br.com.teste.demoteste.service;

import java.util.concurrent.CompletableFuture;

import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.digitalpetri.modbus.master.ModbusTcpMaster;
import com.digitalpetri.modbus.master.ModbusTcpMasterConfig;
import com.digitalpetri.modbus.requests.WriteSingleRegisterRequest;
import com.digitalpetri.modbus.responses.ReadHoldingRegistersResponse;

import io.netty.buffer.ByteBufUtil;
import io.netty.util.ReferenceCountUtil;

@Service
@Transactional
public class MasterService {  
	
	
	public void escritaSimples(int endereco, int valor) {
		ModbusTcpMasterConfig config = new ModbusTcpMasterConfig.Builder("localhost").build();
		ModbusTcpMaster master = new ModbusTcpMaster(config);

		master.connect();

		CompletableFuture<ReadHoldingRegistersResponse> future =
				master.sendRequest(new WriteSingleRegisterRequest(endereco, valor), 0);

		future.thenAccept(response -> {
		    System.out.println("Response: " + ByteBufUtil.hexDump(response.getRegisters()));

		    ReferenceCountUtil.release(response);
		});
		
	}
	
}

